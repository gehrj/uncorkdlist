# README #

This project is hosted at https://gehruncorkdlist.herokuapp.com/  It allows you to create a list with no duplicate entries. It also allows
you to enter queries to have private rooms. These rooms can have their own lists that persist over time. The queries are also removed from
the url and stored in a session. Too see an example of how the queries are to be formmatted visit https://gehruncorkdlist.herokuapp.com/?code=007
so basically /?code="enter room name here". This project was made to demonstrate my skills across the fullstack as well as hosting via cloud.


