'use strict'

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const models = require('./models');
const routes = require('./routes');
const path = require('path');
let port = process.env.PORT || 3000;
let app = express();

// this is the middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));

// I am serving up static files here
app.use(express.static('./public'));
app.use('/bulma',express.static(path.resolve(__dirname, 'node_modules/bulma')))
app.use('/api', routes);
app.get('*', function(req,res) {
    res.sendFile(path.resolve(__dirname, 'public/index.html'));
});

// sending any api route over to routes

// syncing database and starting server
models.db.sync({force: true})
.then(function() {
    app.listen(port, function() {
        console.log('Listening on port ' + port);
    });
})
.catch(console.error);