import React from 'react'

const Hero = (props) => {
    return (
        <section className="hero is-primary">
            <div className="hero-body">
                <div className="container">
                    <h3 className="title is-3">{props.text}</h3>
                </div>
            </div>
        </section>
    )
}

export default Hero