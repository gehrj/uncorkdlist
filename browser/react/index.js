'use strict'

import React from 'react';
import ReactDOM from 'react-dom';
import ListPage from './ListPage.jsx';

ReactDOM.render(<ListPage/>, document.getElementById('app'));