'use strict'

import React from 'react';
import axios from 'axios';
import Hero from './components/Hero';

export default class ListPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            entries: [],
            dataEntry: '',
            code: '',
            codeId: '',
        }
        // binding this value here
        this.handleDataEntry = this.handleDataEntry.bind(this);
        this.onDataSubmit = this.onDataSubmit.bind(this);
        this.postEntry = this.postEntry.bind(this);
        this.getCodeInfo = this.getCodeInfo.bind(this);
        this.duplicateCheck = this.duplicateCheck.bind(this);
        this.sortData = this.sortData.bind(this);
    }
    // in the componentWillMount I am either creating a new code or using an existing code to fetch that users data, code is stored in a session
    componentWillMount() {
        let address = window.location.href;
        history.replaceState({}, queryRemover(address), '/');
        if(address.includes('?code=')) {
            let code = queryParser(address);
            sessionStorage.setItem('code', code);
            this.setState({
                code
            })
            axios.get(`/api/code/${code}`)
            .then(response => {
                let codeId = response.data[0].id
                this.getCodeInfo(codeId);
            })
        }
        else {
            if(sessionStorage.getItem('code')) {
                let code = sessionStorage.getItem('code');
                this.setState({
                    code
                })
                axios.get(`/api/code/id/${code}`)
                .then(response => {
                    this.getCodeInfo(response.data.id)
                })
            }
            else {
                let code = createCode();
                sessionStorage.setItem('code', code);
                this.setState({
                    code
                })
                axios.get(`/api/code/${code}`)
            }
        }
    }
    // updating entry in real time
    handleDataEntry(e) {
        let dataEntry = e.target.value;
        this.setState({
            dataEntry
        })
    }
    // this adds data to our backend when it is triggered through onclick event which is in my render section
    onDataSubmit() {
        let codeId = this.state.codeId;
        let code = this.state.code;
        let entries = this.state.entries;
        let dataEntry = this.state.dataEntry;
        if(codeId) {
            this.postEntry(codeId);
        }
        else {
            axios.get(`/api/code/id/${code}`)
            .then(response => {
                this.postEntry(response.data.id);
            })
        }
    }
    // basically a helper function to make code more dry, this is where we use our backend api to create data in backend
    postEntry(codeId) {
        axios.post(`/api/list/${codeId}`, {
                entry: this.state.dataEntry,
                codeId
            })
            .then(response => {
                this.setState({
                    entries: this.state.entries.concat(this.state.dataEntry),
                    dataEntry: ''
                })
                this.refs.entryField.value = '';
            })
            .catch(error => {
                console.log(error);
            })
    }
    // this is another helper function to make code dry
    getCodeInfo(codeId) {
        if(codeId) {
                    axios.get(`/api/list/${codeId}`)
                    .then(response => {
                        if(!response.data.length) {
                            this.setState({
                                codeId
                            })
                        }
                        else {
                            response.data.forEach(elem => {
                                this.setState({
                                    entries: this.state.entries.concat(elem.entry),
                                    codeId,
                                })
                            })
                        }
                    })
                }
    }
    // this function checks for duplicate entries for specific user
    duplicateCheck() {
        return this.state.entries.includes(this.state.dataEntry);
    }
    // this function will sort the data on the page
    sortData() {
        this.setState({
            entries: this.state.entries.sort()
        })
    }

    render() {
        let data = this.state.entries;
        let entry = this.state.dataEntry;
        let warning = '';
        // I am setting error warnings here
        if(this.duplicateCheck()) warning = 'This Entry Already exists!'
        return (
                <div>
                    <Hero text="Uncorkd List"/>
                    <div className="section">
                        <div className="container">
                            <div className="columns">
                                <div className="column is-2"/>
                                <div className="column">
                                    <div className="card">
                                        <div className="card-content">
                                            <div className="content">
                                                <h3> Your code: {this.state.code} </h3>
                                                    <ul>
                                                    {
                                                    data && data.map(entry => (
                                                        <li className="dataEntries"> Data: {entry} </li>
                                                    )) 
                                                    }
                                                    </ul>
                                                <div className='field has-addons'>
                                                    <div>
                                                        <span className="prefix"> New: </span>
                                                    </div>
                                                    <div className="control">
                                                        <input ref = "entryField" className="input" type="text" placeholder="Enter Data Here" onChange={this.handleDataEntry}>
                                                        </input>
                                                    </div>
                                                    <div className="control">
                                                        { console.log(this.whiteSpaceChecker(entry))
                                                            warning || !entry ? 
                                                            <a className="button is-info" disabled={warning}>
                                                            Save Entry
                                                            </a> :
                                                            <a className="button is-info" onClick={this.onDataSubmit}>
                                                            Save Entry
                                                            </a>
                                                        }
                                                    </div>
                                                    <div className="control">
                                                        <a className="button is-success" onClick={this.sortData}>
                                                            Sort Entries
                                                        </a>
                                                    </div>
                                                    <div>
                                                        { warning && <div className="warning"> {warning} </div>}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="column is-2"/>
                            </div>
                        </div>
                    </div>
                    <div className="fix-footer">
                        <div className="footer">
                            <div className="container">
                                <div className="columns">
                                    <div className="column is-12 has-text-centered">
                                        <div className="content">
                                            <p className="test">
                                                &copy; Uncorkd List 2017
                                            </p>
                                            <p>
                                                Made with Love by Justin Gehr
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )

        }
    }
// this function parses our address and returns just the query code
function queryParser(address) {
    let index = address.indexOf('?code=')
    return address.slice(index + 6);
}
// this function removes our query and returns url without it
function queryRemover(address) {
    let index = address.indexOf('?code=')
    return address.slice(0,index);
}
// this function generates a random hexidecimal code if no query code is entered
function createCode() {
    let code = '';
    let hex = 'abcdef0123456789'
    for(let i = 0; i < 32; i++) {
        code += hex.charAt(Math.floor(Math.random() * hex.length));
    }
    return code;
}