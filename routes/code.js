const express = require('express');
const router = express.Router();
const models = require('../models');
const Code = models.Code;
module.exports = router;

router.get('/:code', (req,res,next) => {
    Code.findOrCreate({ where: {name: req.params.code}})
    .then(code => {
        res.send(code);
    })
})

router.get('/id/:code', (req,res,next) => {
    Code.findOne({ where: {name: req.params.code}})
    .then(code => {
        res.send(code);
    })
})