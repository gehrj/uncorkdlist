const express = require('express');
const router = express.Router();
const list = require('./list');
const code = require('./code');

// routing our routes to correct locations
router.use('/list', list);
router.use('/code', code);

module.exports = router;