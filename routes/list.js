'use strict'

const express = require('express');
const router = express.Router();
const models = require('../models');
const List = models.List;
module.exports = router;

// gets data for specific code
router.get('/:codeId', (req,res,next) => {
    List.findAll({ where: {codeId: req.params.codeId}})
    .then(entries => {
        console.log('what is data?????', entries)
        if(!entries) {
            const error = Error('No entries found');
            err.status = 404;
            throw err;
        }
        res.json(entries);
    });
})
// will add data to specific code
router.post('/:codeId', (req,res,next) => {
    List.create(req.body)
    .then(entry => res.status(201).json(entry))
    .catch(next)
})