const chai = require('chai');
const expect = require('chai').expect;
const models = require('../models');
const Code = models.Code;

// implemented one test, I did not really have time until the past 2 days to work on this project or else I would have been able to implement more

describe('Code Model', () => {
    beforeEach(() => {
        code = Code.build({
            name: '007'
        });
    });
    describe('Name is correct', () => {
        it('code.name should be 007', () => {
            expect(code.name).to.equal('007');
        });
    });
});