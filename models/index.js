'use strict'

const Sequelize = require('sequelize');

let db = new Sequelize(process.env.DATABASE_URL || 'postgres://localhost:5432/Uncorkd', {
    logging: false
});

let List = db.define('list', {
    entry: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

let Code = db.define('code', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    }
})
// this is what makes the relation between code and list, it is a one to many relationship
Code.hasMany(List);

module.exports = {
    db,
    List,
    Code
}